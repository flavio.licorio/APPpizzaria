#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, requests
url = 'http://localhost:3000/Pizzas'
payload =   {
            "Pizza": "Quatro Queijos",
            "Ingredintes": "Queijo muçarela, Prato, Gorgonzola e Parmesão",
            "Preco": 39.9
            }
r = requests.post(url, data=json.dumps(payload), headers = {'Content-type': 'application/json'})