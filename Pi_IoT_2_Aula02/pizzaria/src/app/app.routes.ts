import { Routes, CanActivate, RouterModule } from '@angular/router';
import { FormComponent } from '../app//form/form.component';
import { ListaComponent } from './lista/lista.component';
import { BebidasListComponent } from './bebidas-list/bebidas-list.component';
import { BebidasFormComponent } from './bebidas-form/bebidas-form.component';
import { PedidosListComponent } from './pedidos-list/pedidos-list.component';
import { PedidosFormComponent } from './pedidos-form/pedidos-form.component';


export const appRoutes : Routes = [
    { path: 'pizza', component: FormComponent },
    { path: 'lista-pizza', component: ListaComponent},
    { path: 'lista-bebidas', component: BebidasListComponent},
    { path: 'form-bebidas', component: BebidasFormComponent},
    { path: 'lista-pedidos', component: PedidosListComponent},
    { path: 'form-pedidos', component: PedidosFormComponent}

];

export const AppRoutes = RouterModule.forRoot(appRoutes, { useHash: true });