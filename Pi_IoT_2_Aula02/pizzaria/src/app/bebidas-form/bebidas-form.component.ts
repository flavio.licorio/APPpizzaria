import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bebidas-form',
  templateUrl: './bebidas-form.component.html',
  styleUrls: ['./bebidas-form.component.css']
})
export class BebidasFormComponent implements OnInit {

	public bebida = {};

  constructor(
  	private apiService: ApiService,
    private router: Router
    ) {
   }

  ngOnInit() {
  }

  salvar() {
    this.apiService.inserirBebidas(this.bebida).subscribe(data => {
      console.log("inseriu");
      this.router.navigate(['/lista-bebidas']);
    }, error => {
      console.log("error " + error);
    });
  }

}
