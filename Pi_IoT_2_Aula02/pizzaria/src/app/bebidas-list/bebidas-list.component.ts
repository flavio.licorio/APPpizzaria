import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service'

@Component({
  selector: 'app-bebidas-list',
  templateUrl: './bebidas-list.component.html',
  styleUrls: ['./bebidas-list.component.css']
})
export class BebidasListComponent implements OnInit {

  public lista: Array<any> = [];
  constructor(
  	private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.getBebida().subscribe((data : Array<any>) => {
      this.lista = data;
      console.log(this.lista);
    })
  }

  remover(bebidaID) {
    this.apiService.removerBebidas(bebidaID).subscribe(data => {
      console.log("removeu");
      location.reload();
    }, error => {
      console.log(error);
    })
  }
}
