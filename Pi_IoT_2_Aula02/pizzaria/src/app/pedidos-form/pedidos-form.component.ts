import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pedidos-form',
  templateUrl: './pedidos-form.component.html',
  styleUrls: ['./pedidos-form.component.css']
})
export class PedidosFormComponent implements OnInit {
	
	public pedido = {};
  public rafael: Array<any> = [];
  public samuel: Array<any> = [];

  constructor(
  	private apiService: ApiService,
    private router: Router
    ) { }

  ngOnInit() {
    this.apiService.getBebida().subscribe((data : Array<any>) => {
      this.rafael = data;
      console.log(this.rafael);
      console.log("novo");
    })
    this.apiService.getPizza().subscribe((data : Array<any>) => {
      this.samuel = data;
    }, error => {
      console.log(error);
    })

  }

  salvar() {
    console.log(JSON.stringify(this.pedido));

    this.apiService.inserirPedidos(this.pedido).subscribe(data => {
      console.log();
      this.router.navigate(['/lista-pedidos']);
    }, error => {
      console.log("error " + error);
    });
  }

}
