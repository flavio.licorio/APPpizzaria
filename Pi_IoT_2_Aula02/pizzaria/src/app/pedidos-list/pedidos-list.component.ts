import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pedidos-list',
  templateUrl: './pedidos-list.component.html',
  styleUrls: ['./pedidos-list.component.css']
})
export class PedidosListComponent implements OnInit {
	public pedido: Array<any> = [];

  constructor(
    private apiService: ApiService,
    private router: Router
  	) { }

  ngOnInit() {
  	this.apiService.getPedidos().subscribe((data : Array<any>) => {
      this.pedido = data;
    }, error => {
      console.log(error);
    })
  }
}
